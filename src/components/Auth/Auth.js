import {Button, Form, Row} from 'react-bootstrap'

export const Auth = () => {

    return (
        <Row>
            <div className='container auth_card'>
                <div className='auth_desc'>
                    <div className='desc_title'>Личный кабинет</div>
                    <div className='desc_button'>Зарегистрироваться</div>
                </div>
                <div className='auth_form_block'>
                    <Form.Group className='auth_form'>
                        <Form.Control type="text" placeholder="Логин"/>
                        <br/>
                        <Form.Control type="text" placeholder="Пароль"/>
                        <br/>
                        <div className='auth_remember'>
                            <input type="checkbox" className="custom-checkbox" id="happy" name="happy" value="yes"/>
                            <label htmlFor="happy">Запомнить меня</label>
                        </div>
                        <Button variant="success" className='auth_button'>Войти</Button>
                    </Form.Group>
                </div>
            </div>
        </Row>
    )
}
