import {Col, Container, Row} from 'react-bootstrap'
import birds from './assets/images/birds_2.png'
import logo from './assets/images/logo 1.png'
import {Auth} from "./components/Auth/Auth";

function App() {
    return (
        <div className='main-wrapper'>
            <Container>
                <img src={birds} alt="birds" className='background_birds'/>
                <Row>
                    <Col xs={3}/>
                    <Col>
                        <Row>
                            <div className='main-logo-block'>
                                <div><img src={logo} alt="logo" className='constanta_logo'/></div>
                                <div className='main-block-text'>Дети в семье — <br/>это наша константа</div>
                            </div>
                        </Row>
                        <Auth/>
                    </Col>
                    <Col xs={3}/>
                </Row>
            </Container>
        </div>
    );
}

export default App;
